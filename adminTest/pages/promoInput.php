<?php 
	include('headerPage.php');
	include('sidebarPage.php')
?>
	
<!-- Content Wrapper. Contains page content -->
<!-- =============================================== -->

<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>
		Promo
		<small>Travode Application</small>
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Insert</li>
	  </ol>
	</section>
	<!-- ========================================================================================================== -->
	<!-- Main content -->
	<section class="content">
	  <!-- Default box -->
	  <div class="box">
		<div class="box-body">
		 <button type="submit" class="btn btn-primary " id="btnadd" name="btnadd"><i class="fa fa-plus"></i> Add Promo</button>
		 <br>
		 <br>
		 <div class="box-body" style="max-width:900px;" >
		  <table id="table_promo" class="table table-striped table-bordered table-hover">
			<thead>
			  <tr class="tableheader">
				<th style="width:40px">#</th>
				<th style="width:140px">Title</th>
				<th style="width:140px">Image</th>
				<th style="width:140px">Content</th>
				<th style="width:140px">Start</th>
				<th style="width:140px">Expired</th>
				<th></th>
			  </tr>
			</thead>
			<tbody>
			</tbody>
		  </table>
		 </div><!-- /.box-body -->
		</div>
	  </div><!-- /.box -->

	  <div id="modalcust" class="modal">
		<div class="modal-dialog modal-md">
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">×</button>
			  <h4 class="modal-title">Form Master Promo</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
			  <div class="pad" id="infopanel"></div>
			  <div class="form-horizontal">
				<div class="form-group"> 
				  <label class="col-sm-3  control-label">Title</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="txttitle" placeholder="Title">
					  <input type="hidden" id="crudmethod" value="N"> 
					  <input type="hidden" id="txtid" value="0">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3  control-label">Image</label>
					<div class="col-sm-9">
					  <input type="file" class="form-control" id="imageurl">
					  <p class="help-block">Upload Image</p>
					</div>
				</div>
									
				<div class="form-group">
					<label class="col-sm-3  control-label">Date range</label>
					<div class="col-sm-9">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input type="text" class="form-control pull-right" id="date">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<div class="form-group"> 
				  <label class="col-sm-3  control-label">Category Select</label>
				  <div class="col-sm-9">
					  <select class="form-control" id="categorytxt">
						<option>1</option>
					  </select>
				  </div>
				</div>
				
				<div class="form-group"> 
				  <!-- /.box -->
				  <div class="box box-info">
					<div class="box-header">
					  <h3 class="box-title">Content Promo
						<small>Advanced and full of features</small>
					  </h3>
					  <!-- tools box -->
					  <div class="pull-right box-tools">
						<button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
						  <i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
						  <i class="fa fa-times"></i></button>
					  </div>
					  <!-- /. tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body pad">
					  <form>
							<textarea id="editor1" name="editor1" rows="10" cols="80">
							</textarea>
					  </form>
					</div>
				  </div>
				</div>
				
				<div class="form-group"> 
				  <label class="col-sm-3  control-label"></label>
				  <div class="col-sm-9">
					<button type="submit" class="btn btn-primary " id="btnsave"><i class="fa fa-save"></i> Save</button></div>
				</div>
			  </div>
			  <!--modal footer-->
			</div>
			<!--modal-content-->
		  </div>
		  <!--modal-dialog modal-lg-->
		</div>
		<!--form-kantor-modal-->
	   </div>
	</section><!-- /.content -->

	<!-- ========================================================================================================== -->

</div><!-- /.content-wrapper -->
<?php include('footerPage.php')?>
  