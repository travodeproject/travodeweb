$(document).ready( function () 
    {
      $('#table_promo').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": false,
        "responsive": true,
        "autoWidth": false,
        "pageLength": 10,
        "ajax": {
          "url": "promo/data.php",
          "type": "POST"
        },
        "columns": [
        { "data": "urutan" },
        { "data": "title" },
        { "data": "imageUrl" },
        { "data": "content" },
        { "data": "start" },
		{ "data": "expired" },
        { "data": "button" },
        ]
      });
    });
    $(document).on("click","#btnadd",function(){
        $("#modalcust").modal("show");
        $("#txttitle").focus();
        $("#txttitle").val("");
        $("#imageUrl").val("");
        CKEDITOR.instances['editor1'].setData("");
		$("#categorytxt").val("");
		$("#date").val("");	
        $("#crudmethod").val("N");
        $("#txtid").val("0");
    });
    $(document).on( "click",".btnhapus", function() {
      var id_promo = $(this).attr("id_promo");
      var title = $(this).attr("title");
      swal({   
        title: "Delete Promo?",   
        text: "Delete Promo : "+title+" ?",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",   
        closeOnConfirm: true }, 
        function(){   
          var value = {
            promoId: id_promo
          };
          $.ajax(
          {
            url : "promo/delete.php",
            type: "POST",
            data : value,
            success: function(data, textStatus, jqXHR)
            {
              var data = jQuery.parseJSON(data);
              if(data.result ==1){
                $.notify('Successfull delete promo');
                var table = $('#table_promo').DataTable(); 
                table.ajax.reload( null, false );
              }else{
                swal("Error","Can't delete promo data, error : "+data.error,"error");
              }

            },
            error: function(jqXHR, textStatus, errorThrown)
            {
             swal("Error!", textStatus, "error");
            }
          });
        });
    });
    $(document).on("click","#btnsave",function(){
      var id_promo = $("#txtid").val();
      var title = $("#txttitle").val();
      var imageUrl = $("#imageurl").val();
      var content = CKEDITOR.instances['editor1'].getData();
      var category = $("#categorytxt").val();
	  var date = $("#date").val();
	  var arr = date.split('-');
	  alert(arr[0]);
	  alert(arr[1]);
      var crud=$("#crudmethod").val();
      if(title == '' || title == null ){
        swal("Warning","Please fill promo name","warning");
        $("#txttitle").focus();
        return;
      }
      var value = {
        promoId: id_promo,
        title: title,
        imageUrl:imageUrl,
        content:content,
        category:category,
		start:arr[0],
		expired:arr[1],
        crud:crud
      }
      $.ajax(
      {
        url : "promo/save.php",
        type: "POST",
        data: value,
        success: function(data, textStatus, jqXHR)
        {
		  //console.log(JSON.stringify(value));
          var data = jQuery.parseJSON(data);
          if(data.crud == 'N'){
            if(data.result == 1){
              $.notify('Successfull save data');
              var table = $('#table_promo').DataTable(); 
              table.ajax.reload( null, false );
              $("#txttitle").focus();
              $("#txttitle").val("");
              $("#imageUrl").val("");
              $("#editor1").val("");
			  $("#categorytxt").val("");
			  $("#date").val("");
              $("#crudmethod").val("N");
              $("#txtid").val("0");
              $("#txttitle").focus();
            }else{
              swal("Error","Can't save promo data, error : "+data.error,"error");
            }
          }else if(data.crud == 'E'){
            if(data.result == 1){
              $.notify('Successfull update data');
              var table = $('#table_promo').DataTable(); 
              table.ajax.reload( null, false );
              $("#txttitle").focus();
            }else{
             swal("Error","Can't update promo data, error : "+data.error,"error");
            }
          }else{
            swal("Error","invalid order","error");
          }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
           swal("Error!", textStatus, "error");
        }
      });
    });
    $(document).on("click",".btnedit",function(){
      var id_promo=$(this).attr("id_promo");
      var value = {
        promoId: id_promo
      };
      $.ajax(
      {
        url : "promo/get_cust.php",
        type: "POST",
        data : value,
        success: function(data, textStatus, jqXHR)
        {
          var data = jQuery.parseJSON(data);
		  
		  $("#crudmethod").val("E");
		  $("#txtid").val(data.promoId);
		  $("#txttitle").val(data.title);
		  $("#imageUrl").val(data.imageUrl);
		  CKEDITOR.instances['editor1'].setData(data.content);
		  $("#categorytxt").val(data.categoryId);
		  $("#date").val(data.start +" - "+ data.expired);

          $("#modalcust").modal('show');
          $("#txttitle").focus();
		  $.notify('Successfull update data');
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
          swal("Error!", textStatus, "error");
        }
      });
    });
    $.notifyDefaults({
      type: 'success',
      delay: 500
    });