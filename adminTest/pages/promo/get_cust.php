<?php
include "../../dbConnect/konekDb.php";

$id_promo=$_POST['promoId'];
$query=mysqli_query($con,"select * from promo where promoId=$id_promo");
$array = array();
while($data = mysqli_fetch_array($query,MYSQLI_ASSOC)){
	$array['promoId']= $data['promoId'];
	$array['title']= $data['title'];
	$array['imageUrl']= $data['imageUrl'];
	$array['content']= $data['content'];
	$array['categoryId']= $data['categoryId'];
	$array['start']= date("d/m/Y", strtotime($data['start']));
	$array['expired']= date("d/m/Y", strtotime($data['expired']));
}
echo json_encode(utf8ize($array));

function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}

?>