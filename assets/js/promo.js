$(document).ready( function () 
    {
      var id_promo=$(this).attr("id_promo");
      var value = {
        promoId: id_promo
      };
      $.ajax(
      {
        url : "assets/script/get_promo.php",
        type: "POST",
        data : value,
        success: function(data, textStatus, jqXHR)
        {
          var data = jQuery.parseJSON(data);
          $.each( data, function(i){
            $("#promoItem").append(
                "<div class='col-sm-3 col-xs-6'>\
                  <div class='who'>\
                    <img src='"+data[i].imageUrl+"' alt='' class='img-responsive who-img'>\
                    <h3>\
                      "+data[i].title+"\
                    </h3>\
                    <p class='who-detail'>\
                      <div class='list-detail'>\
                        "+data[i].content+"\
                      </div>\
                    </p>\
                  </div>\
                </div>"
            );
          });
          $.notify('Successfull update data');
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
          swal("Error!", textStatus, "error");
        }
      });
    });

    $.notifyDefaults({
      type: 'success',
      delay: 500
    });